FROM nginx:latest
COPY ./public_html /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]
